

// Arithmetic Operators
let x = 1397;
let y = 7831;

// Additional Operator
let sum = x + y;
console.log("Result of addition operator: " + sum);

// Subtraction Operator
let difference = x - y;
console.log("Result of the subtraction operator: " + difference);

// Multiplication Operator
let product = x * y;
console.log("Result of the multiplication operator: " + product);

// Division Operator
let quotient = x / y;
console.log("Result of the division operator: " + quotient);

// Modulo Operator
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Assignment Operator
// Basic Assignment Operator(=)
// The assignment operator adds the value of the right operand to a variable and assigns the result to the varibale
let assignmentNUmber = 8;

// Addition Assignment Operator
assignmentNUmber += 2;
console.log("Result of the addition assignment operator: " + assignmentNUmber);

// Subtraction/Multiplication/Division (-=, *=, /=)
assignmentNUmber -=2; 
console.log("Result of the subtraction assignment operator: " + assignmentNUmber); //8

// Multiplication Assignment Operator
assignmentNUmber *= 2; 
console.log("Result of the subtraction assignment operator: " + assignmentNUmber); //16

// Division Assignment Operator
assignmentNUmber /= 2; 
console.log("Result of the division assignment operator: " + assignmentNUmber); //8

// Multiple Operators and Parenthesis
/*
	When multiple operators are applied in a single statement, it follows the PEMDAS Rule (Parenthesis, Exponent, Multiplication, Division, Addition, Subtraction)
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Increment and decrement
// Operator that add or subtract values by 1 and reassigns the valueof the varibale where the increment/decrement was applied to

let z = 1;

// Pre-Increment
// let preIncrement = ++z; //2
// console.log("Result of the pre-increment: " + preIncrement);
// console.log("Result of the pre-increment: " + z);

// Post-Increment
let postIncrement = z++;
console.log("Result of the post-increment: " + postIncrement);
console.log("Result of the postIncrement: " + z);

/*
	Pre-increment - adds 1 first before reading value
	Post-increment - reads value first before adding 1
*/

let a = 2;

// Pre-Decrement
// The value "a" is decreased by a value of 1 before returning the value and storing it in the variable "preDecrement";
// let preDecrement = --a;
// console.log("result of the pre-decrement: " + preDecrement);
// console.log("Result of the pre-decrement: " + a);

// Post-Decrement
let postDecrement = a--;
console.log("Result of the postDecrement: " + postDecrement);
console.log("Result of the postDecrement: " + a);

// Post-Decrement
// The value "a" is returned and stored in variable "postDecrement" then the value of "a" is decreased by 1;

// Type Coercion
/*
	The coercion is the automatic or implicit conversion of values from one data type to another.
*/

let numA = "10"; //String
let numB = 12; //Number

/*
	Adding/concatenating a string and a number will result as a string
*/

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion); //String

let coercion1 = numA - numB;
console.log(coercion1); //-2
console.log(typeof coercion1); //Number

// Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Addition of Number and Boolean
/*
	The result is a number
	The boolean "true" is associated with the value of 1
	The boolean "false" is associated with the value of 0
*/

let numE = true + 1;
console.log(numE); //2
console.log(typeof numE); //number

let numF = false + 1;
console.log(numF);

// Comparison Operator
let juan = 'juan';

// Equality Operator(==)
/*
	- Checks whether the operands are equal/have the same content
	- Attempts to CONVERT and COMPARE operands of different data types
	- Returns a boolean value (true / false)
*/

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(1 == true);

// compare two strings that are the same
console.log('juan' == 'juan'); //True
console.log('true' == true); //False
console.log(juan == 'juan'); //True

// Inequality Operator(!=)
/*
	- Checks whether the operands are not equal/have different content
	- Attempts to CONVERT and COMPARE operands of diff data types
*/
console.log(1 != 1); //False
console.log(1 != 2); //True
console.log(1 != '1'); //False
console.log(1 != true); //False
console.log('juan' != 'juan'); //False
console.log('juan' != juan); //False

// Strict Equality Operator (===)
/*
	- Checks whether the operands are equal/have the same content
	- Also COMPARES the data types of 2 values
*/

console.log(1 === 1); //True
console.log(1 === 2); //False
console.log(1 === '1'); //False
console.log(1 === true); //False
console.log('juan' === 'juan'); //True
console.log('juan' === juan); //True

// Strict Inequality Operator (!==)
/*
	Checks whether the operands are not equal/have the same content
	Also COMPARES the data types of 2 values
*/

console.log(1 !== 1); //False
console.log(1 !== 2); //True
console.log(1 !== '1'); //True
console.log(1 !== true); //True
console.log('juan' !== 'juan'); //False
console.log('juan' !== juan); //False

// Relational Operator
// Returns a boolean value
let j = 50;
let k = 65;

// GT/Greater than operator (>)
let isGreaterThan = j > k;
console.log(isGreaterThan); //false

// LT/Less than operator (<)
let isLessThan = j < k;
console.log(isLessThan); //true

// GTE /Greater than or Equal operator (>=)
let isGTorEqual = j >= k;
console.log(isGTorEqual); //false

// LTE /Less than or Equal operator (<=)
let isLTorEqual = j <= k;
console.log(isLTorEqual); //true

let numStr = "30";
// forced coercion to change the string to a number
console.log(j > numStr); //true

let str = "thirty";

console.log(j > str); //false

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& - Double Ampersand)
// Returns true if all operands are true
// true && true = true
// true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet);

// Logical OR operator (|| - Double Pipe)
// Returns true if one of the operands is true
// true || true = true
// true || false = true
// false || false = false
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet); //True

// Logical NOT Operator (! - Exclamation point)
// Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsNotMet); //True